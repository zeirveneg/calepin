<p align="center">
    <img src="data/icons/hicolor/scalable/apps/banner.png"  alt="calepin banner" width="700">
</p>

Calepin is an application for taking and managing notes. Notes are written with markdown, and stored into a single (JSON) file to allow easy backup and syncing. Calepin's interface is simple and uncluttered, with powerful search capabilities and a tags-based management.

Calepin is based on Gtk and written in python. It is still under development. Use it at your own risks!

# Features

- [x] Markdown syntax
- [x] Powerful search
- [x] Colored tags
- [x] Math support (MathJax)
- [x] Light and dark themes 

# Screenshots

## Light theme

<img src="data/screenshots/calepin_view_panel_light.png" alt="View notes" width="30%">
<img src="data/screenshots/calepin_edit_panel_light.png" alt="Edit note" width="30%">
<img src="data/screenshots/calepin_start_panel_light.png" alt="Get started" width="30%">

## Dark theme

<img src="data/screenshots/calepin_view_panel.png" alt="View notes" width="30%">
<img src="data/screenshots/calepin_edit_panel.png" alt="Edit note" width="30%">
<img src="data/screenshots/calepin_start_panel.png" alt="Get started" width="30%">

# Installation

## Building from source with Meson
### Prerequesites
+ python >= 3.7.3
+ gtk >= 3.20
+ git
+ meson >= 0.46
+ webkit2gtk >= 2.22
+ gtksourceview >= 3.20
+ gtkspell >= 3.0
+ mathjax
+ python-markdown
+ python-gtkspellcheck

*Note:* older versions may work, but untested

```
git clone https://gitlab.gnome.org/zeirveneg/calepin.git/

cd calepin

meson build && cd build

ninja install
```
