# Standard library modules
import os
# External modules
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GObject, GtkSource, Gdk, Gio, GLib
from gi.repository import WebKit2
import json
# Calepin modules
from .note import Notes
from .notes_list import ListNotes, ListItem
from .viewers import Editor, Viewer
from .tagseditor import TagsEditor
from .start_panel import StartPanel
from .main_panel import MainPanel


class MainWindow(Gtk.ApplicationWindow):
    """
    Main application window for Calepin

    Extends:
        Gtk.ApplicationWindow
    """

    def __init__(self, *args, settings=None, **kwargs):
        """[summary]

        [description]

        Arguments:
            *args -- arguments to pass to Gtk.ApplicationWindow
            **kwargs -- arguments to pass to Gtk.ApplicationWindow

        Keyword Arguments:
            settings -- instance of the settings' calepin class
        """
        # The AppWindow class will inherit from the instance
        # of the class ApplicationWindow, created from Glade xml file
        super().__init__(*args, **kwargs)
        self.set_application(kwargs['application'])
        self.parent = kwargs['application']

        # Load Glade file and components
        builder = Gtk.Builder()
        GObject.type_register(GtkSource.View)
        GObject.type_register(WebKit2.WebView)
        GObject.type_register(WebKit2.Settings)
        builder.add_from_resource("/org/gnome/Calepin/main_window.ui")
        self.about_dialog = builder.get_object('AboutDialog')
        self.header_bar = builder.get_object('HeaderBar')
        self.main_stack = builder.get_object('main_stack')

        # Help overlay
        builder.add_from_resource("/org/gnome/Calepin/keyboard_shortcuts.ui")
        kshorts = builder.get_object('shortcuts_overview')
        self.set_help_overlay(kshorts)

        # CSS styling
        screen = Gdk.Screen.get_default()
        gtk_provider = Gtk.CssProvider()
        gtk_provider.load_from_resource('/org/gnome/Calepin/css/calepin.css')
        gtk_context = Gtk.StyleContext()
        gtk_context.add_provider_for_screen(screen,
                                            gtk_provider,
                                            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        # Get dark or bright theme
        gtk_settings = Gtk.Settings.get_default()
        dark = settings.get_use_dark_theme()
        gtk_settings.set_property("gtk-application-prefer-dark-theme", dark)

        # Window-specific events
        # - Quitting events
        self.connect('delete-event', self.on_quit)

        # Class attributes
        self.ui_builder = builder
        self.current_panel = None
        self.app_prefs = settings

        # Build action groups
        self.add_file_actions()
        self.add_note_actions()
        self.add_view_actions()
        self.add_win_actions()

        # Initialize panels
        self.startpanel = StartPanel(self, self.ui_builder, self.app_prefs)
        self.startpanel.connect('file_chosen', self.on_file_chosen)
        self.mainpanel = MainPanel(self, self.ui_builder, self.app_prefs)

    def present(self):
        """
        Present the application window to the user
        """
        self.assemble_window()
        self.assemble_header_bar()
        # Check if a file was previously opened
        if os.path.exists(self.app_prefs.get_current_file()):
            # If yes, then assemble the main panel
            self.assemble_main_panel(self.app_prefs.get_current_file())
            last_uid = self.app_prefs.get_last_note_uid()
            self.current_panel.select_note(last_uid)
        else:
            # Otherwise, show the start panel
            self.assemble_start_panel()
        # Load previous window state
        self.resize(*self.app_prefs.get_current_window_size())
        self.move(*self.app_prefs.get_current_window_position())
        self.show()

    def assemble_window(self):
        """
        Setup the main application window by adding to it the
        main application frame from the UI file.
        """
        main_frame = self.ui_builder.get_object('overlay1')
        self.add(main_frame)

    def assemble_header_bar(self):
        """
        Setup the header bar of the main app window
        """
        header_bar = self.ui_builder.get_object('HeaderBar')
        self.set_titlebar(header_bar)

    def assemble_main_panel(self, filename):
        """
        Setup the main panel, where notes are displayed and edited.

        Arguments:
            filename {str} -- path to notes file to load
        """
        self.mainpanel.load_file(filename)
        self.mainpanel.enable_main_actions()
        self.mainpanel.display()
        self.current_panel = self.mainpanel

    def assemble_start_panel(self):
        """
        Setup the startup panel, where a notes file can be chosen
        or created.
        """
        self.startpanel.display()
        self.current_panel = self.startpanel

    def add_view_actions(self):
        """
        Creates the actions for modifying how notes are viewed. Creates the
        associated accelerators, and groups the actions into the "view" group.
        """
        # Create action group
        view_actions = Gio.SimpleActionGroup()

        # Zoom in action
        zoom_in_action = Gio.SimpleAction.new('zoomin', None)
        self.set_accels_for_action("view.zoomin", ["<Control>plus"])
        view_actions.add_action(zoom_in_action)

        # Zoom out action
        zoom_out_action = Gio.SimpleAction.new('zoomout', None)
        self.set_accels_for_action("view.zoomout", ["<Control>minus"])
        view_actions.add_action(zoom_out_action)

        # Zoom default action
        zoom_deflt_action = Gio.SimpleAction.new('zoom-default', None)
        view_actions.add_action(zoom_deflt_action)

        # Add action group to window
        self.insert_action_group('view', view_actions)

    def add_note_actions(self):
        """
        Creates the actions for modifying note properties. Creates the
        associated accelerators, and groups the actions into the "notes" group.
        """
        # Create action group
        note_actions = Gio.SimpleActionGroup()
        # Alias
        gfalse = GLib.Variant.new_boolean(False)

        # Delete a note
        delete_action = Gio.SimpleAction.new("delete", None)
        note_actions.add_action(delete_action)
        self.set_accels_for_action("notes.delete", ["<Control>d"])

        # Mark note as favourite
        starred_action = Gio.SimpleAction.new_stateful("starred", None, gfalse)
        note_actions.add_action(starred_action)
        self.set_accels_for_action("notes.starred", ["<Control>f"])
        # Set starring button iconic
        starring_button = self.ui_builder.get_object('StarringButton')
        starring_button.set_property('iconic', True)
        starring_button.set_property('icon',
                                     Gio.ThemedIcon.new('starred-symbolic'))

        # Edit a note
        edit_action = Gio.SimpleAction.new_stateful('edit', None, gfalse)
        self.set_accels_for_action("notes.edit", ["<Control>e"])
        note_actions.add_action(edit_action)
        # Set edit button iconic
        edit_button = self.ui_builder.get_object('edit_button')
        edit_button.set_property('iconic', True)
        edit_button.set_property('icon',
                                 Gio.ThemedIcon.new('document-edit-symbolic'))

        # Validate the edition a note
        validate_edit_action = Gio.SimpleAction.new('validate-edit', None)
        self.set_accels_for_action("notes.validate-edit",
                                   ["<Control><Shift>e"])
        note_actions.add_action(validate_edit_action)

        # Toggle between edit and preview frames in editor mode
        toggle_edit_preview_action = Gio.SimpleAction.new(
            'toggle-edit-preview', None)
        self.set_accels_for_action("notes.toggle-edit-preview",
                                   ["<Control>Tab"])
        note_actions.add_action(toggle_edit_preview_action)

        # Create new note
        add_action = Gio.SimpleAction.new_stateful('add', None, gfalse)
        self.set_accels_for_action("notes.add", ["<Control>n"])
        note_actions.add_action(add_action)
        # Set add button iconic
        add_button = self.ui_builder.get_object('add_button')
        add_button.set_property('iconic', True)
        add_button.set_property('icon',
                                Gio.ThemedIcon.new('list-add-symbolic'))

        # Tags edition action
        tags_action = Gio.SimpleAction.new('tagsedit', None)
        note_actions.add_action(tags_action)

        # Add action group to window
        self.insert_action_group('notes', note_actions)

    def add_file_actions(self):
        """
        Creates the actions for saving and closing files. Creates the
        associated accelerators, and groups the actions into the "file" group.
        """
        # Create action group
        file_actions = Gio.SimpleActionGroup()

        # Save notes to file
        save_action = Gio.SimpleAction.new("save")
        self.set_accels_for_action("file.save", ["<Control>s"])
        file_actions.add_action(save_action)

        # Close notes file action
        close_file_action = Gio.SimpleAction.new('close-file')
        close_file_action.connect('activate', self.on_close_file)
        self.set_accels_for_action("file.close-file", ["<Control>w"])
        file_actions.add_action(close_file_action)

        # Add action group to window
        self.insert_action_group('file', file_actions)

    def add_win_actions(self):
        """
        Creates actions related to window actions, e.g. showing the
        keyboard shortcuts window.
        """
        # Keyboard shortcuts info action
        shortcuts_action = Gio.SimpleAction.new('shortcuts', None)
        shortcuts_action.connect('activate', self.on_keyboard_shortcuts_window)
        self.add_action(shortcuts_action)

    def _write_current_settings(self):
        # window size
        size = self.get_size()
        self.app_prefs.set_current_window_size(size)
        # window position
        position = self.get_position()
        self.app_prefs.set_current_window_position(position)

    def __getattr__(self, attr):
        return getattr(self.parent, attr)

    def on_keyboard_shortcuts_window(self, *args):
        """
        Show the keyboard shortcuts window
        """
        dialog = self.get_help_overlay()
        dialog.present()

    def on_file_chosen(self, *args):
        """
        Handle file choosing in start panel.

        Event handler. When a file is chosen in start panel, processes
        the new file choice and, if correct, opens it in the main panel.
        """
        filename = args[1]
        # try to open file
        root, ext = os.path.splitext(filename)
        if os.path.exists(filename):
            try:
                self.assemble_main_panel(filename)
            except json.decoder.JSONDecodeError:
                dbuilder = Gtk.Builder()
                dbuilder.add_from_resource(
                    "/org/gnome/Calepin/unsaved_dialog.ui")
                dialog = dbuilder.get_object('CouldNotOpenDialog')
                dialog.set_transient_for(self)
                dialog.run()
                dialog.destroy()
        else:
            with open(filename, 'w') as file:
                json.dump({}, file)
            self.assemble_main_panel(filename)

    def on_close_file(self, *args):
        """
        Handle file closing in main panel.

        Event handler. When a notes file is closed, closes the file, shuts down
        the main panel, saves stuff to preferences and shows up the start panel
        """
        self.app_prefs.add_recent_file(
            self.current_panel.viewer_panel.listnotes.notes.current_target)
        self.on_quit()
        self.current_panel.on_close()
        self.assemble_start_panel()

    def on_quit(self, *args):
        """
        Handle closing of main application window

        Event handler. When main window is closed, checks that there are
        not unsaved changes. If so, show a dialog that warns the users and
        allows them to save changes. Save current app state to settings file.
        """
        if self.current_panel is not None:
            if self.current_panel.__name__ == '__mainpanel__':
                lnotes = self.current_panel.viewer_panel.listnotes
                if lnotes.has_unsaved_changes:
                    dbuilder = Gtk.Builder()
                    dbuilder.add_from_resource(
                        "/org/gnome/Calepin/unsaved_dialog.ui")
                    unsaved_dialog = dbuilder.get_object('UnsavedWarning')
                    response = unsaved_dialog.run()
                    if response == Gtk.ResponseType.YES:
                        self.current_panel.viewer_panel.listnotes.save()
                    unsaved_dialog.destroy()
        if self.current_panel is not None:
            self.current_panel.save_settings()
        # Save window-related settings
        self._write_current_settings()
