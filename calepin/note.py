# Standard library modules
import json
import decimal
import datetime
import re
# from json_encoder import CalepinJSONEncoder, calepin_object_hook

template_note = {
    'title': 'Title',
    'created': datetime.datetime.now(),
    'last_modified': datetime.datetime.now(),
    'tags': [],
    'status': 'normal',  # normal, starred, deleted
    'summary': 'Summary',
    'content': '',
    'uid': 0,
}


class Note(object):
    """
    Base class for the note object:
        - Note properties (title, tags, etc.) are implemented as class
          properties
        - Fuzzy search function is implemented via the search method
    """

    def __init__(self, **kwargs):
        # set attributes corresponding to note properties and given as
        # keyworded arguments
        for key, item in kwargs.items():
            if key in template_note.keys():
                if isinstance(item, type(template_note[key])):
                    setattr(self, key, item)

        # check for missing note properties and sets them to default values
        for key, item in template_note.items():
            if not hasattr(self, key):
                print('WARNING: ', key, 'was not found. Set to default value.')
                setattr(self, key, item)

        # define regular expressions for fuzzy search
        self.title_search = re.compile(r'title:(?P<search>[^ ]*)')
        self.tag_search = re.compile(r'tag:(?P<search>[^ ]*)')
        self.content_search = re.compile(r'content:(?P<search>[^ ]*)')
        self.from_date_search = re.compile(r'from:(?P<search>[^ ]*)')
        self.to_date_search = re.compile(r'to:(?P<search>[^ ]*)')
        self.date_regex = re.compile(
            r'(?P<day>[0-9]{2})[\./-](?P<month>[0-9]{2})[\./-](?P<year>[0-9]{4})')

    def generate_summary(self):
        """
        Generate a summary of the note (now, just the first non-blank line)
        for fancy display in list.
        """
        found = False
        for line in self.content.splitlines():
            if line != '' and line[0] != '!':
                found = True
                break
        if found:
            length = len(line)
            return line[0:min(length, 550)]
        else:
            return ''

    def search(self, search_string):
        """
        Implement fuzzy search of note

        .. rules::
            - search 'items' are separated by spaces
            - search specifiers are 'title:', 'content:', 'tag:'
            - regex strings can in principle be passed (untested)
        """
        # Split search string and generate sub searches
        words = search_string.split()
        # No search string case
        if not words:
            return True
        # More than one search word: atomization needed
        elif len(words) > 1:
            matches = []
            for word in words:
                matches.append(self.search(word))
            return all(matches)
        # If single search word, atomic search is used
        else:
            return self._atomic_search(words[0])

    def _atomic_search(self, search_string):
        """
        Search for a particular string
        """
        string, properties = self._parse_search_string(search_string)
        found = self._search_properties(string, properties)
        return found

    def _parse_search_string(self, search_string):
        """
        Parse search string to extract, if present, search specifiers
        """
        all_properties = ['title', 'tags', 'content']
        # default search string, no specifiers
        search = (search_string, all_properties)

        # title search, 'title:' specifier
        match = self.title_search.match(search_string)
        if match is not None:
            search = (match.group('search'), ['title'])

        # tags search, 'tag:' specifier
        match = self.tag_search.match(search_string)
        if match is not None:
            search = (match.group('search'), ['tags'])

        # content search, 'content:' specifier
        match = self.content_search.match(search_string)
        if match is not None:
            search = (match.group('search'), ['content'])

        # from date search: 'from:' specifier
        match = self.from_date_search.match(search_string)
        if match is not None:
            search = (match.group('search'), ['datefrom'])

        # to date search: 'to:' specifier
        match = self.to_date_search.match(search_string)
        if match is not None:
            search = (match.group('search'), ['dateto'])

        return search

    def _search_properties(self, search_string, properties):
        """
        Search for a search string (regex) in the fields of the note defined
        in properties
        """
        # Generate search re's from search string
        pattern = re.compile(search_string, re.MULTILINE | re.IGNORECASE)
        #
        search_results = []
        for prop in properties:
            if hasattr(self, '_search_' + prop):
                if 'date' in prop:
                    search_result = getattr(self, '_search_' +
                                            prop)(search_string)
                else:
                    search_result = getattr(self, '_search_' + prop)(pattern)
                search_results.append(search_result)
        return any(search_results)

    def _search_title(self, regex):
        """
        Search for a given regular expression in title
        """
        if regex.search(self.title) is not None:
            return True
        else:
            return False

    def _search_content(self, regex):
        """
        Search for a given regular expression in content
        """
        if regex.search(self.content) is not None:
            return True
        else:
            return False

    def _search_tags(self, regex):
        """
        Search for a given regular expression in tags
        """
        found = False
        for tag in self.tags:
            if regex.search(tag.name) is not None:
                found = True
                break
        return found

    def _search_datefrom(self, string):
        """
        Search for a given start date in creation date
        """
        found = False
        correct, date = self._extract_date(string)
        if correct and self.created.date() >= date:
            found = True
        return found

    def _search_dateto(self, string):
        """
        Search for a given start date in creation date
        """
        found = False
        correct, date = self._extract_date(string)
        if correct and self.created.date() <= date:
            found = True
        return found

    def _extract_date(self, string):
        """
        Extract date from a search string
        """
        found = False
        date = None
        match = self.date_regex.fullmatch(string)
        if match is not None:
            day = int(match.group('day'))
            month = int(match.group('month'))
            year = int(match.group('year'))
            date = datetime.date(year, month, day)
            found = True
        return found, date


template_tag = {
    'name': 'None',
    'color': 'rgb(0,0,0)',
    'uid': 0,
}


class Tag(object):

    def __init__(self, **kwargs):
        for key, item in kwargs.items():
            if key in template_tag.keys():
                if isinstance(item, type(template_tag[key])):
                    setattr(self, key, item)

        # check for missing note properties and sets them to default values
        for key, item in template_tag.items():
            if not hasattr(self, key):
                print('WARNING: ', key, 'was not found. Set to default value.')
                setattr(self, key, item)

    def serialize(self, spec_type='tag'):
        tags_dict = {'name': self.name, 'color': self.color, 'uid': self.uid}
        return {'val': tags_dict, '_spec_type': spec_type}

    def __eq__(self, a):
        if isinstance(a, self.__class__):
            match = self.name == a.name
            match = match and self.color == a.color
            match = match and self.uid == a.uid
            if match:
                return True
            else:
                return False
        else:
            return False

    def __ne__(self, a):
        return not self.__eq__(a)


class CalepinJSONEncoder(json.JSONEncoder):
    """
    Custom JSON encoder so that date and time are serializable into json
    """
    # transform objects known to JSONEncoder here
    # def encode(self, o, *args, **kw):
    #     for_json = o
    #     if isinstance(o, Tags):
    #         for_json = o.serialize(spec_type='tags')
    #     return super(CalepinJSONEncoder, self).encode(for_json, *args, **kw)

    def default(self, obj):
        if isinstance(obj, (datetime.datetime,)):
            return {"val": obj.isoformat(), "_spec_type": "datetime"}
        elif isinstance(obj, (decimal.Decimal,)):
            return {"val": str(obj), "_spec_type": "decimal"}
        elif isinstance(obj, (Tag,)):
            return obj.serialize()
        else:
            return super(CalepinJSONEncoder, self).default(obj)


CONVERTERS = {
    'datetime': datetime.datetime.fromisoformat,
    'decimal': decimal.Decimal,
    'tag': Tag
}


def calepin_object_hook(obj):
    _spec_type = obj.get('_spec_type')
    if not _spec_type:
        return obj

    if _spec_type in CONVERTERS:
        if isinstance(obj['val'], (dict,)):
            return CONVERTERS[_spec_type](**obj['val'])
        elif isinstance(obj['val'], (list,)):
            return CONVERTERS[_spec_type](*obj['val'])
        else:
            return CONVERTERS[_spec_type](obj['val'])
    else:
        raise Exception('Unknown {}'.format(_spec_type))


class Tags(list):

    def __init__(self, parent, *args):
        list.__init__(self, args)
        self.parent = parent

    def generate_sublist_from_names(self, note):
        names_sublist = [name for name in note.tags]
        sublist = Tags(note)
        names = [tag.name for tag in self]
        for item in names_sublist:
            try:
                i = names.index(item)
                sublist.append(self[i])
            except ValueError:
                pass
        return sublist

    def _add(self, tag):
        if isinstance(tag, (Tag,)):
            self.append(tag)
        else:
            raise TypeError('argument must be instance of Tag')

    def delete(self, tag):
        try:
            self.remove(tag)
        except ValueError:
            pass
        try:
            for item in self.parent:
                if tag in item.tags:
                    print('Tag found in ', item.title)
                    item.tags.remove(tag)
        except TypeError:
            pass

    def modify(self, tag, new_tag):
        if isinstance(tag, (Tag,)):
            self[self.index(tag)] = new_tag
            for item in self.parent:
                if tag in item.tags:
                    i = item.tags.index(tag)
                    item.tags[i] = new_tag
        else:
            raise TypeError('argument must be instance of Tag')

    def new_tag(self, **kwargs):
        uids = [item.uid for item in self]
        if uids:
            nuid = max(uids) + 1
        else:
            nuid = 0
        tag = Tag(uid=nuid, **kwargs)
        self._add(tag)
        return tag


class Notes(list):
    """
    Ensemble of notes from a given file, in the form of a
    python list.
    """

    def __init__(self, *args):
        list.__init__(self, *args)
        self.current_target = ''
        self.tagslist = Tags(self)

    def save(self, target):
        """
        Save data.
        """
        # Generate notes dictionary
        notes_dict = {}
        for note in self:
            note_dict = {}
            for key in template_note.keys():
                value = getattr(note, key)
                # Tags is a special problem
                if key == 'tags':
                    newval = [tag.name for tag in value]
                    note_dict.update({key: newval})
                else:
                    note_dict.update({key: value})
            notes_dict.update({note.title: note_dict})

        with open(target, 'w') as file:
            json.dump({'Notes': notes_dict, 'Tags': self.tagslist},
                      file, cls=CalepinJSONEncoder, indent=4)

    def load(self, target):
        """
        Load notes for a target .json file.
        Each note is a dictionary entry. For each note,
        a Note class instance is created, and appended
        to the list.
        """
        self.current_target = target
        with open(target, 'r') as file:
            fulldata = json.load(file, object_hook=calepin_object_hook)

        # Load the notes, unless there are none
        try:
            notes_dict = fulldata['Notes']
            for key in notes_dict.keys():
                note_obj = Note(**notes_dict[key])
                self.append(note_obj)
        except KeyError:
            pass

        # Load the tags
        self.tagslist = Tags(self)
        try:
            tags_list = fulldata['Tags']
            for value in tags_list:
                self.tagslist.append(value)
        except KeyError:
            pass

        # Replace tag strings in notes by tag objects
        for note in self:
            note.tags = self.tagslist.generate_sublist_from_names(note)

    def new_note(self):
        """
        Create a new note from template_note. The properties
        created and last_modified are set as now. Unique id (uid)
        is set as the max value of the uids in the list + 1.
        """
        newnote = Note()
        # Set creation time
        now = datetime.datetime.now()
        newnote.created = now
        newnote.last_modified = now
        # Generate unique note ID
        uids = [note.uid for note in self]
        if uids:
            newnote.uid = max(uids) + 1
        else:
            newnote.uid = 0
        # Tagslist
        newnote.tags = Tags(newnote)
        # Append to notes list
        self.append(newnote)

        return newnote

    def get_by_uid(self, uid):
        for note in self:
            if note.uid == uid:
                return note
        return None

    # def get_tags_list(self):
    #     tagslist = []
    #     for note in self:
    #         for tag in note.tags:
    #             if tag not in tagslist:
    #                 tagslist.append(tag)
    #     return tagslist

    def update_tags(self):
        # Check for deleted tags
        new_uids = [tag.uid for tag in self.tagslist]
        for note in self:
            for tag in note.tags:
                if tag not in new_uids:
                    note.tags.remove(tag)
                elif tag in new_uids and tag not in self.tagslist:
                    new_tag = self.tagslist[new_uids.index(tag.uid)]
                    note.tags.remove(tag)
                    note.tags.append(new_tag)


if __name__ == '__main__':
    notes = Notes()
    # notes.load('notes.json')
    # for note in notes:
    #     print(note.title)
    #     print(note.created)
    #     print(note.last_modified)
    # notes.save(notes.current_target)
    # tag = Tag('myriad')
    dummy_tags = Tags(**{'test':{'name': 'blah'}, 'test2':{'name': 'blah2'}})
