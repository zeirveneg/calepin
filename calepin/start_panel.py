from os import path
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject
from .settings import application_settings


class StartPanel(GObject.GObject):
    __gsignals__ = {
        'file_chosen': (GObject.SIGNAL_RUN_FIRST, None,
                        (str,))
    }
    __name__ = '__startpanel__'

    def __init__(self, parent, builder, app_prefs):
        GObject.GObject.__init__(self)
        self.parent = parent
        self.ui_builder = builder
        self.app_prefs = app_prefs
        browse_button = builder.get_object('browse_button')
        browse_button.connect('clicked', self.on_browse_files)
        self.new_file_button = self.ui_builder.get_object('new_file_button')
        self.new_file_button.connect("clicked", self.on_create_new_file)

    def display(self):
        self._assemble_header_bar()
        main_stack = self.ui_builder.get_object('main_stack')
        main_stack.set_visible_child_name('empty_page')
        self.enable_startup_actions_only()
        self.fill_recent_files()

    def enable_startup_actions_only(self):
        actions_group_names = ['file', 'notes', 'view']
        for action_group_name in actions_group_names:
            action_group = self.parent.get_action_group(action_group_name)
            if action_group is not None:
                for action_name in action_group.list_actions():
                    action_group.lookup_action(action_name).set_enabled(False)

    def _assemble_header_bar(self):
        item = self.ui_builder.get_object('add_button')
        item.hide()
        item = self.ui_builder.get_object('StarringButton')
        item.hide()
        item = self.ui_builder.get_object('edit_button')
        item.hide()
        item = self.ui_builder.get_object('NoteMenuButton')
        item.hide()
        self.new_file_button.show()

    def fill_recent_files(self):
        recent_files_listbox = self.ui_builder.get_object('recent_files')
        recent_files_listbox.connect('row-activated', self.on_recent_file_clicked)
        # Clean stuff first
        # Remove listboxrow entries
        for row in recent_files_listbox.get_children():
            recent_files_listbox.remove(row)
        recent_files_listbox.queue_draw()

        files = application_settings.get_recent_files()
        for file in files:
            # Parse filename
            location, filename = path.split(file)
            row = Gtk.ListBoxRow()
            grid = Gtk.Grid()
            grid.get_style_context().add_class('recent_files_grid')
            row.get_style_context().add_class('recent_files_item')
            filename_label = Gtk.Label(filename, expand=True,
                                       halign=Gtk.Align.START)
            filename_label.get_style_context().add_class('recent_files_name')
            location_label = Gtk.Label(location, expand=True,
                                       halign=Gtk.Align.START)
            location_label.get_style_context().add_class('recent_files_location')
            # Add title and location to grid
            grid.attach(filename_label, 0, 0, 1, 1)
            grid.attach(location_label, 0, 1, 2, 1)
            row.add(grid)
            recent_files_listbox.add(row)
        # Show
        recent_files_listbox.show_all()

    def on_recent_file_clicked(self, listbox, listrow):
        items = listrow.get_children()[0].get_children()
        new_target = path.join(items[0].get_label(), items[1].get_label())
        self.emit('file_chosen', new_target)

    def on_create_new_file(self, *args):
        dialog = Gtk.FileChooserDialog(
            action=Gtk.FileChooserAction.SAVE,
            title='Save new file as...',
            parent=self.parent,
            buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                     Gtk.STOCK_SAVE, Gtk.ResponseType.OK)
        )
        dialog.set_do_overwrite_confirmation(True)
        dialog.set_current_name('notes.json')
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            new_target = dialog.get_filename()
            dialog.destroy()
            self.emit('file_chosen', new_target)
        else:
            dialog.destroy()

    def on_browse_files(self, *args):
        dialog = Gtk.FileChooserDialog(
            action=Gtk.FileChooserAction.OPEN,
            title='Open file',
            parent=self.parent,
            buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                     Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        )
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            new_target = dialog.get_filename()
            dialog.destroy()
            self.emit('file_chosen', new_target)
        else:
            dialog.destroy()

    def save_settings(self):
        self.app_prefs.set_last_note_uid(0)
        self.app_prefs.set_current_file('')
