from gi.repository import GObject, Gtk, GtkSource, Gio, GLib


class AppSettings(GObject.GObject):

    spellcheck_language = GObject.Property(type=str, default='en_US')
    lastnoteuid = GObject.Property(type=int, default=0)
    currentfile = GObject.Property(type=str, default='')
    colorscheme_editor = GObject.Property(type=str, default='tango')
    webview_zoom_level = GObject.Property(type=float, default=1.0)
    font_editor = GObject.Property(type=str, default='Cantarell Book 12')
    gui_prefs = ['spellcheck_language', 'use_dark_theme', 'colorscheme_editor',
                 'font_editor']
    recent_files = GObject.Property(type=str, default='')
    use_dark_theme = GObject.Property(type=bool, default=True)

    def __init__(self, settings):
        GObject.GObject.__init__(self)
        self._bind_to_settings(settings)
        self.settings = settings

    def _bind_to_settings(self, settings):
        for prop in self.list_properties():
            settings.bind(prop.name.replace('_', '-'),
                          self,
                          prop.name.replace('_', '-'),
                          Gio.SettingsBindFlags.DEFAULT)

    def on_css_style(self, obj):
        self.webview_theme = obj.get_active_id()

    def on_spellcheck_language(self, obj):
        self.spellcheck_language = obj.get_active_id()

    def on_color_scheme(self, obj, param):
        self.colorscheme_editor = obj.get_style_scheme().get_id()

    def on_font_editor(self, obj):
        self.font_editor = obj.get_font()

    def on_done(self, button):
        # for pref in self.gui_prefs:
            # self.parser.set(self.section, pref, getattr(self, pref))
        button.get_parent_window().destroy()

    def get_current_file(self):
        return self.currentfile

    def set_current_file(self, value):
        self.currentfile = value

    def get_last_note_uid(self):
        return self.lastnoteuid

    def set_last_note_uid(self, value):
        self.lastnoteuid = value

    def get_current_window_size(self):
        return self.settings.get_value('window-size')

    def set_current_window_size(self, window_size):
        g_variant = GLib.Variant('ai', [window_size.width,
                                        window_size.height])
        self.settings.set_value('window-size', g_variant)

    def get_current_window_position(self):
        return self.settings.get_value('window-position')

    def set_current_window_position(self, window_position):
        g_variant = GLib.Variant('ai', [window_position[0],
                                        window_position[1]])
        self.settings.set_value('window-position', g_variant)

    def get_panedview_width(self):
        return self.settings.get_int('panedview-width')

    def set_panedview_width(self, value):
        self.settings.set_int('panedview-width', value)

    def add_recent_file(self, filename):
        recent_files_list = self.recent_files.split(',')
        if filename not in recent_files_list and filename != '':
            recent_files_list.insert(0, filename)
            if len(recent_files_list) > 3:
                recent_files_list.pop()
        self.recent_files = ','.join(recent_files_list)

    def get_recent_files(self):
        return self.recent_files.split(',')

    def get_use_dark_theme(self):
        return self.settings.get_boolean('use-dark-theme')

    def set_use_dark_theme(self, value):
        # Get dark or bright theme
        gtk_settings = Gtk.Settings.get_default()
        gtk_settings.set_property("gtk-application-prefer-dark-theme", value)
        self.settings.set_boolean('use-dark-theme', value)

    def on_use_dark_theme(self, button, value):
        self.set_use_dark_theme(value)

    def present(self, parent_window):
        builder = Gtk.Builder()
        builder.add_from_resource("/org/gnome/Calepin/settings.ui")

        # GUI elements for Preferences popup window
        dialog_prefs = builder.get_object('PreferencesDialog')
        button_done = builder.get_object('DoneButton')
        dark_theme_switch = builder.get_object('DarkThemeSwitch')
        spell_lang = builder.get_object('SpellcheckLanguage')
        color_scheme = builder.get_object('EditorColorScheme')
        font_style = builder.get_object('EditorFontStyle')

        # Set current values
        dark_theme_switch.set_active(self.use_dark_theme)
        spell_lang.set_active_id(self.spellcheck_language)
        scheme_manager = GtkSource.StyleSchemeManager.get_default()
        current_scheme = scheme_manager.get_scheme(self.colorscheme_editor)
        try:
            color_scheme.set_style_scheme(current_scheme)
        except TypeError:
            print('WARNING: no current style found')
        font_style.set_font(self.font_editor)

        # Preference changed events
        dark_theme_switch.connect('state-set', self.on_use_dark_theme)
        spell_lang.connect('changed', self.on_spellcheck_language)
        color_scheme.connect('notify::style-scheme', self.on_color_scheme)
        font_style.connect('font-set', self.on_font_editor)

        # Connect button events
        button_done.connect('clicked', self.on_done)

        # Set transient for parent window
        if parent_window is not None:
            dialog_prefs.set_transient_for(parent_window)

        # Present dialog
        dialog_prefs.present()


application_settings = AppSettings(Gio.Settings.new("org.gnome.Calepin"))
