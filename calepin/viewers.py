# Standard python library
import webbrowser
import copy
import codecs
import datetime
# External python modules
# import mistune
import markdown as md
import mdx_math as mdmath
import gi
gi.require_version('Gtk', '3.0')
from gtkspellcheck import spellcheck
from gi.repository.WebKit2 import PolicyDecisionType, WebInspector
from gi.repository import Gtk, GObject, GtkSource, Pango, WebKit2, Gio, GLib
# Calepin modules
from .tagseditor import TagsPicker


class Editor(GObject.GObject):
    """
    Markdown editor to edit the notes.

    Features:
        - Syntax highlighting through Gtk.LanguageManager
        - Automatic spellcheck with gtkspellcheck
    """
    note_modified = GObject.Property(type=bool, default=False)

    def __init__(self, builder, app_prefs):
        GObject.GObject.__init__(self)
        # Get UI objects from builder
        sourceview = builder.get_object('content_editor')
        main_stack = builder.get_object('main_stack')
        title_editor = builder.get_object('title_editor')
        editor_stack = builder.get_object('editor_stack')
        content_preview = builder.get_object('content_preview')
        preview_settings = builder.get_object('settings_htmlpreview')
        preview = HtmlViewer(content_preview, preview_settings, app_prefs)

        # Tags editor instance
        tags_picker = TagsPicker(builder, app_prefs)

        # Set markdown as language for SourceView
        langman = GtkSource.LanguageManager.get_default()
        mdlang = langman.get_language('markdown')
        # Initialize text buffer
        sourceview_buffer = sourceview.get_buffer()
        sourceview_buffer.set_language(mdlang)
        sourceview.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        # Style text buffer
        styleman = GtkSource.StyleSchemeManager.get_default()
        scheme = styleman.get_scheme(app_prefs.colorscheme_editor)
        sourceview_buffer.set_style_scheme(scheme)
        app_prefs.connect('notify::colorscheme-editor',
                          self.on_colorscheme_changed)
        # Initialize spellcheck
        spell = spellcheck.SpellChecker(sourceview,
                                        language=app_prefs.spellcheck_language)
        # Language for spellcheck from application preferences
        app_prefs.connect('notify::spellcheck-language',
                          self.on_change_spellcheck_lang)
        # Font description from application preferences
        sourceview.modify_font(
            Pango.font_description_from_string(app_prefs.font_editor))
        app_prefs.connect('notify::font-editor', self.on_change_font_style)

        # Class properties
        self.view = sourceview
        self.buffer = sourceview_buffer
        self.main_stack = main_stack
        self.title_editor = title_editor
        self.app_prefs = app_prefs
        self.spell = spell
        self.preview = preview
        self._current_note = None
        self._styleman = styleman
        self.tags_picker = tags_picker
        # Modification handling
        self.buffer.connect('modified-changed', self.on_note_modified)
        self.title_editor.connect('changed', self.on_note_modified)
        self.tags_picker.connect(
            'notify::tags-selection-modified', self.on_note_modified)
        editor_stack.connect('notify::visible-child', self.on_switch)

    def on_switch(self, *args):
        self.preview.update(self._get_content())

    def load_note(self, note, tagslist):
        """
        Load the note instance into the various UI components of the note
        editor (title editor, content editor, tags editor, etc.)
        """
        self._title_just_loaded = True
        self._tags_just_loaded = True
        self._current_note = note

        self.buffer.handler_block_by_func(self.on_note_modified)
        self.title_editor.handler_block_by_func(self.on_note_modified)

        self.buffer.begin_not_undoable_action()
        self.buffer.set_text(note.content)
        self.buffer.set_modified(False)
        self.buffer.end_not_undoable_action()
        self.title_editor.set_text(note.title)
        # Populate tags picker
        tags = note.tags.copy()
        self.tags_picker.set_tags(tags, tagslist)
        # self.tags_label.set_text(','.join([tag.name for tag in note.tags]))

        self.buffer.handler_unblock_by_func(self.on_note_modified)
        self.title_editor.handler_unblock_by_func(self.on_note_modified)

    def _get_content(self):
        """
        Get the content of the note currently lying in the text buffer
        """
        start, end = self.buffer.get_bounds()
        # intralink = r'<a name="lastpost">News</a>'
        # intralink = ''
        # pos = self.buffer.cursor_position
        text = self.buffer.get_text(start, end, True)
        return text

    def write_current_note(self):
        """
        Writes the note properties (title, content, tags, etc.) into the
        note instance given as input parameter.
        """
        self._current_note.content = self._get_content()
        self._current_note.title = self.title_editor.get_text()
        self._current_note.summary = self._current_note.generate_summary()
        self._current_note.tags = self.tags_picker.get_selected_tags()

    def show_in_stack(self):
        """
        Show the note editor page in the display stack
        """
        self.main_stack.set_visible_child_name('editor_page')

    def is_shown_in_stack(self):
        """
        Whether the editor is the active slide of the stack
        """
        if self.main_stack.get_visible_child_name() == 'editor_page':
            return True
        else:
            return False

    def on_note_modified(self, *args):
        self._current_note.last_modified = datetime.datetime.now()
        self.note_modified = True

    def reset_modified_trigger(self):
        self.buffer.handler_block_by_func(self.on_note_modified)
        self.buffer.begin_not_undoable_action()
        self.buffer.set_modified(False)
        self.buffer.end_not_undoable_action()
        self.buffer.handler_unblock_by_func(self.on_note_modified)

    def on_change_spellcheck_lang(self, *args):
        """
        Change the spellcheck language
        Callback function for notify::spellcheck-language signal, emited
        when preferences are changed.
        """
        self.spell.language = self.app_prefs.spellcheck_language

    def on_colorscheme_changed(self, *args):
        new = self._styleman.get_scheme(self.app_prefs.colorscheme_editor)
        self.buffer.set_style_scheme(new)

    def on_change_font_style(self, *args):
        self.view.modify_font(
            Pango.font_description_from_string(self.app_prefs.font_editor))


class Viewer():
    """
    Gtk WebKit2 Html viewer to view the notes.

    Features:
        - Inclusion of custom header and footer to pimp display
        - MathJax is supported
        - Custom CSS themes can be loaded
    """

    default_html_start = """<!doctype HTML><html><body>"""
    default_html_end = """</body></html>"""

    def __init__(self, builder, app_prefs):
        # get GUI elements from builder
        stack = builder.get_object('main_stack')
        webview = builder.get_object('MarkdownWebView')
        websettings = builder.get_object('settings1')
        htmlviewer = HtmlViewer(webview, websettings, app_prefs)

        # Custom html header and footer, if they exist
        try:
            with open('head.html') as f:
                self.html_start = f.read()
            with open('tail.html') as f:
                self.html_end = f.read()
        except IOError:
            self.html_start = self.default_html_start
            self.html_end = self.default_html_end

        # Application preferences
        app_prefs.connect('notify::use-dark-theme', self.on_change_theme)

        # Class properties
        self.webview = webview
        self.stack = stack
        self.app_prefs = app_prefs
        self.htmlviewer = htmlviewer
        # Current note
        self.current_note = None

    def show_in_stack(self):
        """
        Show the note viewer page in the display stack
        """
        self.stack.set_visible_child_name('view_browse_page')

    def on_tags_updated(self, *args):
        """
        Update the note displayed when tags are modified through
        tags editor
        """
        if self.current_note is not None:
            self.update(self.current_note)

    def update(self, note):
        """
        Update the html viewer with the note instance given as input parameter
        """
        self.current_note = note
        mdtxt = self._generate_markdown(note)
        self.htmlviewer.update(mdtxt)

    def _generate_markdown(self, note):
        # tagstxt = '____\n Tags: '
        tagstxt = 'Tags: '
        for tag in note.tags:
            start = ' <span style="color:TAGCOLOR"> \#'.replace('TAGCOLOR', tag.color)
            tagstxt += start + tag.name + '</span>'
        tagstxt += '\n ____ \n'
        # tagstxt += '\n\n'
        titletxt = '#' + note.title + '\n'
        mdtxt = titletxt + tagstxt + note.content
        return mdtxt

    def clear(self):
        self.current_note = None
        self.htmlviewer.update('')

    def on_change_theme(self, *args):
        self.htmlviewer.on_change_theme()


class TagViewerItem(Gtk.FlowBoxChild):

    def __init__(self, tag):
        # ListBoxRow
        Gtk.FlowBoxChild.__init__(self)

        # Main box
        box = Gtk.Box()

        # Label
        label = Gtk.Label('#' + tag.name, expand=False,
                          halign=Gtk.Align.START)
        label.set_width_chars(0)
        label.set_ellipsize(Pango.EllipsizeMode.END)
        label.set_justify(Gtk.Justification.LEFT)

        # Add title, summary and label to grid
        box.pack_start(label, 0, 1, 0)
        self.add(box)

        # CSS styling
        self.get_style_context().add_class('tagsviewer-item')
        new_provider = Gtk.CssProvider()
        str_color = tag.color.replace(')', ',1)').replace('rgb', 'rgba')
        style = '.tagsviewer-item:selected, active{color: ' + str_color + ';}\n'
        str_color = tag.color.replace(')', ',1)').replace('rgb', 'rgba')
        style += '.tagsviewer-item{color: ' + str_color + ';}'
        new_provider.load_from_data(style.encode('UTF-8'))
        self.get_style_context().add_provider(new_provider, 800)

        self.tag = tag


class HtmlViewer():
    """
    Gtk WebKit2 Html viewer to view the notes.

    Features:
        - Inclusion of custom header and footer to pimp display
        - MathJax is supported
        - Custom CSS themes can be loaded
    """

    default_html_start = """<!doctype HTML><html><body>"""
    default_html_end = """</body></html>"""
    base_uri = r'file://' + ''

    def __init__(self, webview, websettings, app_prefs):
        # Set settings of webview
        webview.set_settings(websettings)
        # inspector = webview.get_inspector()
        # inspector.attach()
        # inspector.show()

        css_theme = self._get_css_theme(app_prefs)

        # Custom html header and footer, if they exist
        try:
            # Read head file
            head_file = Gio.File.new_for_uri('resource:///org/gnome/Calepin/html/head.html')
            self.html_start = head_file.load_bytes()[0].get_data().decode('utf-8')
            # Read the CSS file
            target = 'resource:///org/gnome/Calepin/css/' \
                     + css_theme
            css_file = Gio.File.new_for_uri(target)
            self.css_text = css_file.load_bytes()[0].get_data().decode('utf-8')
            # Read tail file
            end_file = Gio.File.new_for_uri('resource:///org/gnome/Calepin/html/tail.html')
            self.html_end = end_file.load_bytes()[0].get_data().decode('utf-8')
        except GLib.Error:
            print('FALLBACK: no custom html head/tail found')
            self.html_start = self.default_html_start
            self.html_end = self.default_html_end
            self.css_text = ''

        # Mistune parser
        # self.markdown = mistune.Markdown()
        # Python-markdown parser
        math_extension = mdmath.MathExtension(add_preview=True,
                                              enable_dollar_delimiter=True
                                              )
        md_extensions = [math_extension, 'markdown_checklist.extension']
        self.markdown = md.Markdown(extensions=md_extensions,
                                    output_format='html5')

        # Zoom level
        webview.set_zoom_level(app_prefs.webview_zoom_level)

        # Application preferences
        app_prefs.connect('notify::use-dark-theme', self.on_change_theme)

        # Navigation policy
        webview.connect('decide-policy', self.on_decide_policy)

        # Class properties
        self.webview = webview
        self.app_prefs = app_prefs
        self.current_txt = None

    def on_decide_policy(self, webview, decision, decision_type):
        # Block outgoing http traffic
        # print('Request: ', decision.get_request().get_uri())
        if decision_type == PolicyDecisionType.NAVIGATION_ACTION:
            uri = decision.get_request().get_uri()
            if 'file://' not in uri:
                decision.ignore()
                webbrowser.open(uri)
            else:
                decision.use()
        else:
            decision.use()
        return False

    def _read_string_from_resource(self, uri):
        try:
            gfile = Gio.File.new_for_uri(uri)
            text = gfile.load_bytes()[0].get_data().decode('utf-8')
        except GLib.Error:
            text = ''
        return text

    def _get_css_theme(self, app_prefs):
        if app_prefs.get_use_dark_theme():
            return 'retro.css'
        else:
            return 'air.css'

    def on_change_theme(self, *args):
        new_theme = self._get_css_theme(self.app_prefs)
        res = 'resource:///org/gnome/Calepin/css/' + new_theme
        self.css_text = self._read_string_from_resource(res)
        if self.current_txt is not None:
            self.update(self.current_txt)

    def update(self, mdtxt):
        """
        Update the html viewer with the note instance given as input parameter
        """
        # Generate html from markdown with python-markdown parser
        htmltext = self.markdown.convert(mdtxt)
        # htmltext = self.markdown(mdtxt) # Mistune parser, if need be

        # Edit html head to include current css theme
        start = self.html_start.replace('PUT_THE_CSS_HERE', self.css_text)
        # with open('_html_temp.html', 'w') as tempfile:
            # tempfile.write(start + htmltext + self.html_end)
        # Load the html into webview
        # self.webview.load_uri(self.base_uri+'_html_temp.html')
        html = start + htmltext + self.html_end
        self.webview.load_html(html, base_uri='file://')
        self.current_txt = mdtxt

    def zoom_in(self, *args):
        current_zoom_level = self.webview.get_zoom_level()
        self.webview.set_zoom_level(current_zoom_level + 0.1)

    def zoom_out(self, *args):
        current_zoom_level = self.webview.get_zoom_level()
        self.webview.set_zoom_level(current_zoom_level - 0.1)

    def zoom_default(self, *args):
        self.webview.set_zoom_level(1.0)
