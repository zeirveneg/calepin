# Standard python library
import copy
# External libraries
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango, Gio, GObject, Gdk
# Calepin libraries
from calepin.note import Notes
from calepin.notes_list import ListItem


class TagsPicker(GObject.GObject):
    tags_selection_modified = GObject.Property(type=bool, default=False)

    def __init__(self, builder, app_prefs):
        GObject.GObject.__init__(self)
        # Graphical objects
        self.main_stack = builder.get_object("main_stack")
        self.tags_popover = builder.get_object('tags_popover')
        self.flowbox = builder.get_object('TagsList')
        self.newtag_entry = builder.get_object('NewTagEntry')
        add_button = builder.get_object('AddTagButton')
        show_button = builder.get_object('show_tags')
        self.tags_label = builder.get_object('tags_display')
        # CSS styling
        self.flowbox.get_style_context().add_class('tagslist')
        # Handle selection events
        self.flowbox.connect('child-activated', self._on_select)
        # Add new tag when button clicked
        add_button.connect('clicked', self._on_add_tag)
        # Show popover when button clicked
        show_button.connect('clicked', self._on_show)
        # Other class parameters
        self._current_tagslist = None
        self._tags_selected = None

    def _on_show(self, *args):
        self.tags_popover.show_all()

    def _on_select(self, flowbox, fbchild):
        tag = fbchild.tag
        if tag in self._tags_selected:
            flowbox.unselect_child(fbchild)
            fbchild._on_activate()
            unselected_tag = fbchild.tag
            self._tags_selected.remove(unselected_tag)
            self._on_tagslit_changed()
        else:
            flowbox.select_child(fbchild)
            fbchild._on_activate()
            selected_tag = fbchild.tag
            self._tags_selected.append(selected_tag)
            self._on_tagslit_changed()

    def set_tags(self, tags_selected, tagslist):
        """
        Initial display of the tags list for a given notes list
        and a given selected note
        """
        # Get displayed tags
        for element in self.flowbox.get_children():
            self.flowbox.remove(element)
        # Update tags list if new tags
        for tag in tagslist:
            tag_item = TagItem(tag)
            self.flowbox.add(tag_item)
            tag_item._on_activate()
        # Selects tags of current note
        for item in self.flowbox.get_children():
            if item.tag in tags_selected and not item.is_selected():
                self.flowbox.select_child(item)
                item._on_activate()
            elif item.tag not in tags_selected and item.is_selected():
                self.flowbox.unselect_child(item)
                item._on_activate()

        # self.flowbox.show_all()
        self._current_tagslist = tagslist
        self._tags_selected = tags_selected

        # Update tags display
        self._display_tags()

    def on_tagslist_update(self, *args):
        if self._current_tagslist is not None:
            new_uids = [tag.uid for tag in self._current_tagslist]
            for i, old_tag in enumerate(self._tags_selected):
                if old_tag.uid in new_uids and old_tag not in self._current_tagslist:
                    j = new_uids.index(old_tag.uid)
                    self._tags_selected[i] = self._current_tagslist[j]
            for i, tag in enumerate(self._tags_selected):
                if tag.uid not in new_uids:
                    self._tags_selected.pop(i)
            self.set_tags(self._tags_selected, self._current_tagslist)
            print(self._tags_selected)
            self._on_tagslit_changed()

    def get_selected_tags(self):
        return self._tags_selected

    def _on_tagslit_changed(self, *args):
        """
        Send notify event via GObject property when
        some tag is changed
        """
        self.tags_selection_modified = True
        self.tags_selection_modified = False
        # Updates tags display
        self._display_tags()

    def _display_tags(self):
        displaystr = ''
        for tag in self._tags_selected:
            tagcolor = Gdk.RGBA()
            success = tagcolor.parse(tag.color)
            if success:
                red = int(tagcolor.red * 255)
                green = int(tagcolor.green * 255)
                blue = int(tagcolor.blue * 255)
                colorstr = '#{r:02x}{g:02x}{b:02x}'.format(r=red, g=green, b=blue)
            else:
                colorstr = '#000000'
            markhead = '<span fgcolor=\'' + colorstr + '\'>'
            marktail = '</span>  '
            displaystr += markhead + '#' + tag.name + marktail
        self.tags_label.set_markup(displaystr)

    def _on_add_tag(self, *args):
        """
        Handle addition of a new tag item
        """
        # Get new tag and add to tags list
        newtag = self._current_tagslist.new_tag(
            name=self.newtag_entry.get_text()
        )

        # Clear newtag entry
        self.newtag_entry.set_text('')

        # Create new tag item and put it in flowbox
        tag_item = TagItem(newtag)
        self.flowbox.add(tag_item)

        # Select the newly created tag
        self.flowbox.select_child(tag_item)
        self._tags_selected.append(newtag)

        # Changed state
        self._on_tagslit_changed()

        # Update display
        self.flowbox.show_all()


class TagItem(Gtk.FlowBoxChild):

    def __init__(self, tag):
        # ListBoxRow
        Gtk.FlowBoxChild.__init__(self)

        # Main box
        box = Gtk.Box(spacing=6)

        # Label
        label = Gtk.Label('#' + tag.name, expand=True,
                          halign=Gtk.Align.START)
        label.get_style_context().add_class('tagitem_label')
        label.set_max_width_chars(11)
        label.set_ellipsize(Pango.EllipsizeMode.END)
        label.set_justify(Gtk.Justification.LEFT)

        # Add title, summary and label to grid
        box.pack_start(label, 0, 1, 0)
        icon = Gtk.Image.new_from_icon_name(
            'object-select-symbolic', Gtk.IconSize.BUTTON)
        box.pack_end(icon, 0, 0, 0)
        self.add(box)

        # CSS styling
        self.get_style_context().add_class('tagslist-item')
        new_provider = Gtk.CssProvider()
        str_color = tag.color.replace(')', ',1)').replace('rgb', 'rgba')
        style = '.tagslist-item:selected, active{background-color: ' + \
            str_color + ';}\n'
        str_color = tag.color.replace(')', ',.3)').replace('rgb', 'rgba')
        style += '.tagslist-item{background-color: ' + str_color + ';}'
        new_provider.load_from_data(style.encode('UTF-8'))
        self.get_style_context().add_provider(new_provider, 800)

        self.tag = tag
        self.icon = icon

    def _on_activate(self, *args):
        if self.is_selected():
            self.icon.set_from_icon_name(
                'object-select-symbolic', Gtk.IconSize.BUTTON)
        else:
            self.icon.set_from_icon_name('', Gtk.IconSize.BUTTON)
        self.queue_draw()


class TagsEditor(GObject.GObject):

    tags_modified = GObject.Property(type=bool, default=False)

    def __init__(self, parent, tagslist):
        GObject.GObject.__init__(self)
        self.parent = parent
        self.tagslist = tagslist
        builder = Gtk.Builder()
        builder.add_from_resource("/org/gnome/Calepin/window_tagedit.ui")
        self.dialog = builder.get_object('tags_editor')
        self.text_editor = builder.get_object('TextPicker')
        self.color_editor = builder.get_object('ColorPicker')
        self.flowbox = builder.get_object('flowbox')
        self.flowbox.connect('child-activated', self._on_edit)
        self.stack = builder.get_object('stack')
        self.validate_button = builder.get_object('validate_button')
        self.validate_button.connect('clicked', self._on_validate)
        self.cancel_button = builder.get_object('cancel_button')
        self.cancel_button.connect('clicked', self._on_cancel)
        self.confirm_button = builder.get_object('confirm_changes')
        self.confirm_button.hide()
        self.confirm_button.connect('clicked', self._on_confirm)
        self.refute_button = builder.get_object('refute_changes')
        self.refute_button.hide()
        self.refute_button.connect('clicked', self._on_refute)
        self.delete_button = builder.get_object('delete_button')
        self.delete_button.connect('clicked', self._on_delete)
        # Populate with a deep copy of the tags list
        self._populate(copy.deepcopy(tagslist))
        self._tag_on_edit = None
        # window size
        self.dialog_size = None

    def show(self):
        self.dialog.set_transient_for(self.parent)
        self.dialog.set_modal(True)
        self.dialog.present()
        self.dialog_size = self.dialog.get_size()

    def _populate(self, tagslist):
        for tag in tagslist:
            item = TagEditorItem(tag)
            self.flowbox.add(item)
        self.flowbox.show_all()

    def _on_validate(self, button):
        # Propagate changes
        new_tags = [item.tag for item in self.flowbox.get_children()]
        new_uids = [tag.uid for tag in new_tags]
        for tag in self.tagslist:
            if tag.uid not in new_uids:
                self.tagslist.delete(tag)
            elif tag.uid in new_uids and tag not in new_tags:
                new_tag = new_tags[new_uids.index(tag.uid)]
                self.tagslist.modify(tag, new_tag)
        self.tags_modified = True
        self.dialog.close()

    def _on_cancel(self, button):
        self.dialog.close()

    def _on_edit(self, flowbox, child):
        tag = child.tag
        # set tag name
        self.stack.set_visible_child(self.stack.get_child_by_name('editor'))
        self.text_editor.set_text(tag.name)
        # set tag color
        color = Gdk.RGBA(0, 0, 0, 0)
        color.parse(tag.color)
        self.color_editor.set_rgba(color)
        # modify header bar buttons
        self.cancel_button.hide()
        self.validate_button.hide()
        self.refute_button.show()
        self.confirm_button.show()
        # store the item being edited
        self._tag_on_edit = child
        # store dialog window size so that it can be restored
        self.dialog_size = self.dialog.get_size()

    def _on_refute(self, button):
        self.stack.set_visible_child(self.stack.get_child_by_name('tagslist'))
        # modify header bar buttons
        self.cancel_button.show()
        self.validate_button.show()
        self.refute_button.hide()
        self.confirm_button.hide()
        self._tag_on_edit = None
        # Restore size if it was modified by color editor
        if self.color_editor.get_property('show_editor'):
            self.color_editor.set_property('show_editor', False)
            self.dialog.resize(self.dialog_size.width, self.dialog_size.height)

    def _on_confirm(self, button):
        self._tag_on_edit.tag.name = self.text_editor.get_text()
        self._tag_on_edit.tag.color = self.color_editor.get_rgba().to_string()
        self._tag_on_edit.update()
        self.stack.set_visible_child(self.stack.get_child_by_name('tagslist'))
        # modify header bar buttons
        self.cancel_button.show()
        self.validate_button.show()
        self.refute_button.hide()
        self.confirm_button.hide()
        self._tag_on_edit = None
        # Restore size if it was modified by color editor
        if self.color_editor.get_property('show_editor'):
            self.color_editor.set_property('show_editor', False)
            self.dialog.resize(self.dialog_size.width, self.dialog_size.height)

    def _on_delete(self, button):
        self.stack.set_visible_child(self.stack.get_child_by_name('tagslist'))
        self._tag_on_edit.destroy()
        # modify header bar buttons
        self.cancel_button.show()
        self.validate_button.show()
        self.refute_button.hide()
        self.confirm_button.hide()
        self._tag_on_edit = None


class TagEditorItem(Gtk.FlowBoxChild):

    def __init__(self, tag):
        # ListBoxRow
        Gtk.FlowBoxChild.__init__(self)

        # Main box
        box = Gtk.Box()

        # Label
        label = Gtk.Label(label='#' + tag.name, expand=True,
                          halign=Gtk.Align.START)
        label.get_style_context().add_class('tagitem_label')
        label.set_width_chars(0)
        label.set_ellipsize(Pango.EllipsizeMode.END)
        label.set_justify(Gtk.Justification.LEFT)

        # Add title, summary and label to grid
        box.pack_start(label, 0, 1, 0)
        self.add(box)

        # CSS styling
        self.get_style_context().add_class('tagseditor-item')

        self.tag = tag
        self.label = label

        # Update everything for starters
        self.update()

    def update(self):
        self.label.set_text('#' + self.tag.name)
        new_provider = Gtk.CssProvider()
        str_color = self.tag.color.replace(')', ',1)').replace('rgb', 'rgba')
        style = '.tagseditor-item{background-color: ' + str_color + ';}'
        new_provider.load_from_data(style.encode('UTF-8'))
        self.get_style_context().add_provider(new_provider, 800)

# class TagEditorItem(Gtk.ListBoxRow):

#     def __init__(self, tag, stack):
#         Gtk.ListBoxRow.__init__(self)

#         # Box
#         box = Gtk.Box(spacing=18)
#         # Label
#         label = Gtk.Label(label=tag.name, expand=True,
#                           halign=Gtk.Align.START)
#         # Color button
#         try:
#             color = Gdk.RGBA(0, 0, 0, 0)
#             color.parse(tag.color)
#         except TypeError:
#             colorbutton = Gtk.ColorButton()
#         colorbutton = Gtk.Button.new()
#         colorbutton.connect('clicked', self._on_modify_color)
#         colorbutton.get_style_context().add_class('colorbutton')

#         new_provider = Gtk.CssProvider()
#         new_css_stylesheet = 'button.colorbutton{background-color: blue;}'
#         new_provider.load_from_data(new_css_stylesheet.encode('UTF-8'))
#         colorbutton.get_style_context().add_provider(new_provider, 800)
#         # Delete button
#         deletebutton = Gtk.Button.new_from_icon_name('window-close-symbolic',
#                                                      Gtk.IconSize.BUTTON)
#         deletebutton.connect('clicked', self._on_delete)
#         deletebutton.set_relief(Gtk.ReliefStyle.NONE)

#         # Text edit button
#         editbutton = Gtk.Button.new_from_icon_name('document-edit-symbolic',
#                                                      Gtk.IconSize.BUTTON)
#         editbutton.connect('clicked', self._on_edit)
#         editbutton.set_relief(Gtk.ReliefStyle.NONE)

#         # Pack everything into box
#         box.pack_start(colorbutton, 0, 0, 0)
#         box.pack_start(label, 0, 1, 0)
#         box.pack_start(editbutton, 0, 0, 0)
#         box.pack_start(deletebutton, 0, 0, 0)

#         # CSS styling
#         self.get_style_context().add_class('tagseditor-item')

#         self.add(box)
#         self.tag = tag
#         self.stack = stack
#         self._label = label
#         self._colorbutton = colorbutton

#     def _on_delete(self, button):
#         self.hide()

#     def _on_edit(self, button):
#         pass

#     def _on_modify_color(self, button):
#         self.stack.set_visible_child(self.stack.get_child_by_name('color_picker'))
#         self.stack.get_children()[1].connect('color-activated', self._on_color_picked)

#     def _on_color_picked(self, widget, color):
#         # self.stack.disconnect('')
#         new_provider = Gtk.CssProvider()
#         new_css_stylesheet = 'button.colorbutton{background-color: '+color.to_string()+';}'
#         new_provider.load_from_data(new_css_stylesheet.encode('UTF-8'))
#         self._colorbutton.get_style_context().add_provider(new_provider, 800)
#         self.stack.set_visible_child(self.stack.get_child_by_name('tagslist'))

#     def get_name(self):
#         return self._label.get_text()

#     def get_color(self):
#         return self._colorbutton.get_rgba().to_string()


if __name__ == '__main__':
    notes = Notes()
    notes.load('notes.json')
    # CSS styling
    screen = Gdk.Screen.get_default()
    gtk_provider = Gtk.CssProvider()
    gtk_provider.load_from_path('css/calepin.css')
    gtk_context = Gtk.StyleContext()
    gtk_context.add_provider_for_screen(screen,
                                        gtk_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    tags_editor = TagsEditor(None, notes.tagslist)
    tags_editor.show()
    Gtk.main()
