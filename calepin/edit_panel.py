# Standard library modules
import datetime
import os
# External modules
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GObject, GtkSource, Gdk, Gio, GLib
from gi.repository import WebKit2
# Calepin modules
from .note import Notes
from .viewers import Editor


class EditPanel():

    def __init__(self, parent, ui_builder, app_prefs):
        # Editor instance
        self.editor = Editor(ui_builder, app_prefs)
        # UI
        discard_button = ui_builder.get_object('discard_edition')
        discard_button.connect('clicked', self.on_discard_edition)
        self.stack = ui_builder.get_object('editor_stack')
        #
        self.parent = parent
        # Handle actions
        notes_actions = parent.get_action_group('notes')
        self.edit_action = notes_actions.lookup_action('edit')
        validate_edit_action = notes_actions.lookup_action(
            'validate-edit')
        validate_edit_action.connect('activate', self.on_validate_edition)
        toggle_view_action = notes_actions.lookup_action(
            'toggle-edit-preview')
        toggle_view_action.connect('activate', self.on_toggle_edit_preview)

    def on_validate_edition(self, *args):
        self.editor.write_current_note()
        # Re-enable edit action and set to false
        self.edit_action.set_enabled(True)
        self.edit_action.change_state(GLib.Variant.new_boolean(False))

    def on_discard_edition(self, *args):
        # Re-enable edit action and set to false
        self.edit_action.set_enabled(True)
        self.edit_action.change_state(GLib.Variant.new_boolean(False))

    def on_toggle_edit_preview(self, *args):
        panels = self.stack.get_children()
        current_panel = self.stack.get_visible_child()
        panels.pop(panels.index(current_panel))
        self.stack.set_visible_child(panels[0])
