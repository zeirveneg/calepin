# Standard library modules
import datetime
import os
# External modules
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GObject, GtkSource, Gdk, Gio, GLib
from gi.repository import WebKit2
# Calepin modules
from .note import Notes
from .notes_list import ListNotes, ListItem
from .viewers import Viewer


class ViewerPanel():

    def __init__(self, parent, ui_builder, editor, app_prefs):
        self.viewer = Viewer(ui_builder, app_prefs)
        self.notes_list = ui_builder.get_object('NotesList')
        self.list_search_bar = ui_builder.get_object('list_search_bar')
        self.list_search_bar.connect('search-changed', self.on_search_changed)
        self.listnotes = ListNotes(self.notes_list, editor, self.viewer,
                                   self.list_search_bar)
        self.listnotes.listbox.connect('row-selected', self.on_row_selected)
        self.list_action_bar = ui_builder.get_object('list_action_bar')
        self.favorite_button = ui_builder.get_object('StarringButton')
        self.info_labels = [ui_builder.get_object('CreatedInfoLabel'),
                            ui_builder.get_object('LastmodInfoLabel'),
                            ui_builder.get_object('StatusInfoLabel')]
        #
        self.previous_note_item = None
        search_button = ui_builder.get_object('advanced_search_button')
        search_button.connect('clicked', self.on_start_advanced_search)
        # Hide multiple selection bar
        list_action_bar = ui_builder.get_object('list_action_bar')
        list_action_bar.hide()
        #
        self.ui_builder = ui_builder
        # Actions
        notes_actions = parent.get_action_group('notes')
        self.edit_action = notes_actions.lookup_action('edit')
        self.add_action = notes_actions.lookup_action('add')
        self.delete_action = notes_actions.lookup_action('delete')
        self.star_action = notes_actions.lookup_action('starred')

    def load_file(self, filename):
        self.listnotes.clear()
        self.listnotes.load(filename)
        self.viewer.clear()

    def update(self):
        self.listnotes.update()

    def on_row_selected(self, *args):
        listbox, listrow = args
        rows = listbox.get_selected_rows()
        # Show or hide action bar for multiple selection
        if len(rows) <= 1 and self.list_action_bar.is_visible():
            self.list_action_bar.hide()
            if not self.edit_action.get_enabled():
                self.edit_action.set_enabled(True)
                self.add_action.set_enabled(True)
                self.delete_action.set_enabled(True)
                self.delete_action.set_enabled(True)
        elif len(rows) > 1 and not self.list_action_bar.is_visible():
            self.list_action_bar.show()
            if self.edit_action.get_enabled():
                self.edit_action.set_enabled(False)
                self.add_action.set_enabled(False)
                self.delete_action.set_enabled(False)

        if len(rows) == 0:
            if self.edit_action.get_enabled():
                self.edit_action.set_enabled(False)
                self.delete_action.set_enabled(False)
        elif len(rows) == 1 and not self.edit_action.get_enabled():
            self.edit_action.set_enabled(True)
            self.delete_action.set_enabled(True)
        # Show viewer of the note
        if listrow is not None:
            listrow.on_select()
            self.previous_note_item = listrow

        # Star button stuff
        if len(rows) == 0:
            self.favorite_button.set_property('icon',
                                     Gio.ThemedIcon.new('non-starred-symbolic'))
        elif len(rows) == 1 and rows[0].note.status == 'normal':
            self.favorite_button.set_property('icon',
                                     Gio.ThemedIcon.new('non-starred-symbolic'))
        elif len(rows) == 1 and rows[0].note.status == 'starred':
            self.favorite_button.set_property('icon',
                                     Gio.ThemedIcon.new('starred-symbolic'))
        elif len(rows) > 1:
            self.favorite_button.set_property('icon',
                                     Gio.ThemedIcon.new('non-starred-symbolic'))

        # Info display in note popover menu
        if len(rows) == 0 or len(rows) > 1:
            for label in self.info_labels:
                        label.set_text('')
        elif len(rows) == 1:
            texts = []
            tmp = self._format_datetime_to_string(rows[0].note.created)
            texts.append(tmp)
            tmp = self._format_datetime_to_string(rows[0].note.last_modified)
            texts.append(tmp)
            texts.append(rows[0].note.status)
            for label, text in zip(self.info_labels, texts):
                label.set_text(text)

    def _format_datetime_to_string(self, dt):
        return dt.date().isoformat()

    def clear_note_popmenu(self):
        for label in self.info_labels:
            label.set_text('')

    def on_search_changed(self, search_entry):
        self.listnotes.listbox.invalidate_filter()

    def on_start_advanced_search(self, button):
        today_combo = self.ui_builder.get_object('to_day')
        tomonth_combo = self.ui_builder.get_object('to_month')
        toyear_combo = self.ui_builder.get_object('to_year')
        ok_button = self.ui_builder.get_object('validate_advanced_search')
        advanced_search_popover = self.ui_builder.get_object(
            'advanced_search_popover')

        now = datetime.date.today()
        today_combo.set_value(now.day)
        month = '{0:02}'.format(now.month)
        tomonth_combo.set_active_id(month)
        toyear_combo.set_value(now.year)
        ok_button.connect('clicked', self.on_advanced_search)
        advanced_search_popover.show()

    def on_advanced_search(self, button):

        today_combo = self.ui_builder.get_object('to_day')
        tomonth_combo = self.ui_builder.get_object('to_month')
        toyear_combo = self.ui_builder.get_object('to_year')
        fromday_combo = self.ui_builder.get_object('from_day')
        frommonth_combo = self.ui_builder.get_object('from_month')
        fromyear_combo = self.ui_builder.get_object('from_year')
        advanced_search_popover = self.ui_builder.get_object(
            'advanced_search_popover')
        title_search_entry = self.ui_builder.get_object('title_search')
        content_search_entry = self.ui_builder.get_object('content_search')
        tags_search_entry = self.ui_builder.get_object('tags_search')

        search = []
        search += self._extract_search_entries(title_search_entry,
                                               'title:')
        search += self._extract_search_entries(content_search_entry,
                                               'content:')
        search += self._extract_search_entries(tags_search_entry,
                                               'tag:')
        d = ['{0:0>2d}'.format(today_combo.get_value_as_int()),
             tomonth_combo.get_active_id(),
             '{0:0>4d}'.format(toyear_combo.get_value_as_int())
             ]
        todate = ['to:' + ('.').join(d)]
        d = ['{0:0>2d}'.format(fromday_combo.get_value_as_int()),
             frommonth_combo.get_active_id(),
             '{0:0>4d}'.format(fromyear_combo.get_value_as_int())
             ]
        fromdate = ['from:' + ('.').join(d)]

        search = ' '.join(search + fromdate + todate)
        self.list_search_bar.set_text(search)
        advanced_search_popover.hide()

    def _extract_search_entries(self, entry, specifier=''):
        text = entry.get_text()
        entries = []
        for word in text.split(','):
            if word.strip() != '':
                entries.append(specifier + word)
        return entries
