# application.py
#
# Copyright 2019 Matthieu Genevriez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Standard library modules
import sys
# External modules
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GLib
# Calepin module
from .main_window import MainWindow
from .settings import application_settings


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='org.gnome.Calepin',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.window = None

    def do_startup(self):
        """
        Start the application
        """
        Gtk.Application.do_startup(self)
        GLib.set_application_name('Calepin')
        GLib.set_prgname('Calepin')

        self.assemble_application_actions()

    def do_activate(self):
        """
        Setup the main application window
        """
        if not self.window:
            # window
            self.window = MainWindow(
                application=self, settings=application_settings)
            # global keyboard shortcuts
            self.add_global_accelerators()
            # pass application object to window
            self.window.application = self
        # show window
        self.window.present()

    def assemble_application_actions(self):
        """
        Setup global actions
        """
        # Quit action
        quit_action = Gio.SimpleAction.new('quit', None)
        quit_action.connect('activate', self.on_quit)
        self.add_action(quit_action)

        # Preferences action
        settings_action = Gio.SimpleAction.new('settings', None)
        settings_action.connect('activate', self.show_settings_dialog)
        self.add_action(settings_action)

        # About action
        about_action = Gio.SimpleAction.new('about', None)
        about_action.connect('activate', self.show_about_dialog)
        self.add_action(about_action)

    def add_global_accelerators(self):
        """
        Setup global keyboard shortcuts for global actions
        """
        self.set_accels_for_action("app.open", ["<Control>o"])
        self.set_accels_for_action("app.quit", ["<Control>q"])

    def show_about_dialog(self, *args):
        """
        Application's about dialog
        """
        # Load UI from XML file
        dbuilder = Gtk.Builder()
        dbuilder.add_from_resource("/org/gnome/Calepin/window_about.ui")
        dialog = dbuilder.get_object('AboutDialog')
        # Show dialog
        if self.window is not None:
            dialog.set_transient_for(self.window)
        dialog.present()

    def show_settings_dialog(self, *args):
        """
        Application's settings dialog
        """
        application_settings.present(self.window)

    def on_quit(self, *args):
        """
        Quit application window
        """
        self.window.on_quit()
        self.quit()

    def do_shutdown(self):
        """
        Quit application
        """
        Gtk.Application.do_shutdown(self)
