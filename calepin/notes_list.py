# Standard library modules
import re
import datetime
# External modules
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango, Gdk, GObject
# Calepin modules
from calepin.note import Notes


COLOR_PARSER = re.compile(r'rgb\(([0-9]{1,3}),([0-9]{1,3}),([0-9]{1,3})\)')


class ListNotes(GObject.GObject):

    has_unsaved_changes = GObject.Property(type=bool, default=False)

    def __init__(self, listbox, editor, htmlviewer, searchbar):
        GObject.GObject.__init__(self)

        self.listbox = listbox
        # Sort function
        self.listbox.set_sort_func(self.chronological_and_favorites, None)
        # Filter/search function
        self.listbox.set_filter_func(self.filter_search, None)
        # Header function
        self.listbox.set_header_func(self.headers, None)
        # Header labels
        self.normal_header = Gtk.Label('',
                                       halign=Gtk.Align.START)
        self.normal_header.set_text('All notes')

        self.favorite_header = Gtk.Label('', expand=True,
                                         halign=Gtk.Align.START)
        self.favorite_header.set_text('Favorites')
        # CSS styling
        self.normal_header.get_style_context().add_class('category_header')
        self.favorite_header.get_style_context().add_class('category_header')

        self.notes = Notes()
        self.editor = editor
        self.htmlviewer = htmlviewer
        self.searchbar = searchbar
        # list to register UIDs of all notes deleted during session
        self.deleted_notes = []

        # Connect to modification event from note md editor
        editor.connect('notify::note-modified', self.on_note_modified)

    def on_note_modified(self, obj, prop):
        """
        Callback function to notify::note-modified events from the markdown
        editor.
        """
        if obj.note_modified is True:
            selected = self.listbox.get_selected_rows()
            for item in selected:
                item.changed()
            self.has_unsaved_changes = True

    def select(self, uid=0):
        """
        Select a note in the list given its UID
        """
        for row in self.listbox.get_children():
            if row.note.uid == uid:
                self.listbox.select_row(row)
            elif row.is_selected():
                self.listbox.unselect_row(row)
        self.listbox.queue_draw()

    def get_selected(self, mode='uid'):
        """
        Return the UID of the note currently selected.
        If no note is selected, returns -1
        """
        selected = self.listbox.get_selected_rows()
        if selected:
            if mode == 'uid':
                return selected[0].note.uid
            elif mode == 'note_instance':
                return selected[0].note
        else:
            return -1

    def _update_all_tags(self, obj, prop):
        """
        Update tags when tags were modified
        """
        for item in self.listbox.get_children():
            item.update()
        obj = False
        self.has_unsaved_changes = True
        self.listbox.queue_draw()

    def load(self, target):
        """
        Loads notes from target file and
        generates corresponding list
        """
        # Load notes
        self.notes.load(target)
        # First fills in starred notes:
        starred_notes = filter(lambda x: x.status == 'starred', self.notes)
        for note in starred_notes:
            item = ListItem(note, self.notes.tagslist,
                            self.editor, self.htmlviewer)
            self.listbox.add(item)
        # Fill in normal notes list
        normal_notes = filter(lambda x: x.status in 'normal', self.notes)
        for note in normal_notes:
            item = ListItem(note, self.notes.tagslist,
                            self.editor, self.htmlviewer)
            self.listbox.add(item)
        # Show all widgets
        self.listbox.show_all()

    def clear(self):
        """
        Clean the list of current notes
        """
        # Remove listboxrow entries
        for row in self.listbox.get_children():
            self.listbox.remove(row)
        self.listbox.queue_draw()
        # Re-initializes the notes class instance
        self.notes.__init__()

    def save(self):
        """
        Save all notes if unsaved changes exist
        """
        if self.has_unsaved_changes:
            if self.editor.is_shown_in_stack():
                self.editor.write_current_note()
            self.notes.save(self.notes.current_target)
            self.has_unsaved_changes = False
            self.editor.reset_modified_trigger()

    def add(self):
        """
        Add a new note to the list
        """
        newnote = self.notes.new_note()
        newitem = ListItem(newnote, self.notes.tagslist,
                           self.editor, self.htmlviewer)
        self.listbox.add(newitem)
        self.has_unsaved_changes = True
        self.listbox.show_all()
        return newitem

    def delete(self, rows):
        """
        Remove rows from the list
        """
        for row in rows:
            row.on_delete()
        uids = [row.note.uid for row in rows]
        self.deleted_notes.append(uids)
        self.has_unsaved_changes = True

    def undo_last_delete(self):
        last_uids = self.deleted_notes[-1]
        for last_uid in last_uids:
            note = self.notes.get_by_uid(last_uid)
            note.status = note.status.replace('deleted-', '')
            item = ListItem(note, self.notes.tagslist,
                            self.editor, self.htmlviewer)
            self.listbox.add(item)
            self.has_unsaved_changes = True
            self.listbox.select_row(item)
        self.listbox.show_all()

    def star(self):
        """
        Mark a note as favorite.
        If already favorite, does nothing
        """
        for item in self.listbox.get_selected_rows():
            if item.note.status != 'starred':
                self.has_unsaved_changes = True
                item.on_starred()
                self.listbox.queue_draw()

    def unstar(self):
        """
        Unmark a note from favorites.
        If already not a favorite, does nothing
        """
        for item in self.listbox.get_selected_rows():
            if item.note.status != 'normal':
                self.has_unsaved_changes = True
                item.on_normal()
                self.listbox.queue_draw()

    def filter_search(self, *args):
        """
        Filter function used in listbox. Used to search in the list of notes
        for a specific data.
        Search string is fetched from the search bar.
        """
        row = args[0]
        match = row.note.search(self.searchbar.get_text())
        return match

    def chronological_and_favorites(self, row1, row2, user_data):
        """
        ListBox dynamical sorting function to order notes chronologically.
        Favorites (or starred) notes will be put on top of the list.
        Order is updated each time a note is changed.
        """
        # Date and favorite status (True/False) of the rows to compare
        date1, star1 = row1.note.last_modified, row1.note.status == 'starred'
        date2, star2 = row2.note.last_modified, row2.note.status == 'starred'
        if star1 and star2:
            if date1 > date2:
                return -1
            elif date1 == date2:
                return 0
            elif date1 < date2:
                return 1
        elif star1:
            return -1
        elif star2:
            return 1
        else:
            if date1 > date2:
                return -1
            elif date1 == date2:
                return 0
            elif date1 < date2:
                return 1

    def headers(self, current_row, row_before, user_data):
        """
        Listbox dynamical function to set the Favorites and All headers.
        Header position is updated each time a note is changed.
        """
        if row_before is None:
            star1 = current_row.note.status == 'starred'
            if star1:
                current_header = current_row.get_header()
                if current_header is None:
                    self.favorite_header.destroy()
                    current_row.set_header(self.favorite_header)
                else:
                    if current_header.get_text() != self.favorite_header.get_text():
                        self.favorite_header.destroy()
                        current_row.set_header(self.favorite_header)
            else:
                current_header = current_row.get_header()
                if current_header is not None:
                    if current_header.get_text() != self.normal_header.get_text():
                        self.normal_header.destroy()
                        current_row.set_header(self.normal_header)
                else:
                    self.normal_header.destroy()
                    current_row.set_header(self.normal_header)
        elif row_before is not None:
            star1 = current_row.note.status == 'starred'
            star2 = row_before.note.status == 'starred'
            if star2 and not star1:
                current_header = current_row.get_header()
                if current_header is None:
                    self.normal_header.destroy()
                    current_row.set_header(self.normal_header)
                else:
                    if current_header.get_text() != self.normal_header.get_text():
                        self.normal_header.destroy()
                        current_row.set_header(self.normal_header)
            else:
                if current_row.get_header() is not None:
                    current_row.set_header(None)


class ListItem(Gtk.ListBoxRow):

    def __init__(self, note, tagslist, editor, htmlviewer):
        # ListBoxRow
        Gtk.ListBoxRow.__init__(self)

        # Grid
        grid = Gtk.Grid()
        self.get_style_context().add_class('listitem')

        # Label and summary
        title_label = Gtk.Label(note.title, expand=True,
                                halign=Gtk.Align.START)
        title_label.get_style_context().add_class('listitem_title')
        title_label.set_width_chars(0)
        title_label.set_ellipsize(Pango.EllipsizeMode.END)
        title_label.set_justify(Gtk.Justification.LEFT)
        # Summary label
        summary_label = Gtk.Label(note.summary, expand=True,
                                  halign=Gtk.Align.START)
        summary_label.get_style_context().add_class('listitem_summary')
        # summary_label.set_width_chars(0)
        summary_label.set_ellipsize(Pango.EllipsizeMode.END)
        # Tags label
        tags_label = Gtk.Label(expand=True,
                               halign=Gtk.Align.START)
        tags_label.set_markup(self.generate_tags_string(note))
        tags_label.get_style_context().add_class('listitem_tags')
        tags_label.set_width_chars(0)
        tags_label.set_ellipsize(Pango.EllipsizeMode.END)

        # Add title, summary and label to grid
        grid.attach(title_label, 1, 0, 1, 1)
        grid.attach(summary_label, 1, 1, 2, 1)
        # grid.attach(tags_label, 0, 2, 1, 1)

        # Date of creation
        date_str = self._generate_fancy_date(note.last_modified.date())
        date_label = Gtk.Label(date_str, halign=Gtk.Align.END)
        date_label.get_style_context().add_class('listitem_date')
        grid.attach(date_label, 2, 0, 1, 1)

        # Add a separator at the bottom
        sep = Gtk.Separator()
        grid.attach(sep, 0, 3, 3, 1)

        # Color ribbon
        box = Gtk.Box()
        ribbon = Gtk.DrawingArea.new()
        ribbon.set_property('width-request', 6)
        ribbon.connect('draw', self._on_draw_monochrome_ribbon)
        box.pack_start(ribbon, True, True, 0)
        box.get_style_context().add_class('listitem_ribbon')
        grid.attach(box, 0, 0, 1, 2)

        # Add to listboxrow
        self.add(grid)

        self.title_label = title_label
        self.summary_label = summary_label
        self.tags_label = tags_label
        self.date_label = date_label
        self.ribbon = ribbon

        # Note editor and viewer
        self.editor = editor
        self.htmlview = htmlviewer
        self.note = note
        self.tagslist = tagslist

    def _on_draw_monochrome_ribbon(self, widget, cairo_context):
        rectangle_size = widget.get_allocated_size()[0]
        if self.note.tags:
            color = Gdk.RGBA()
            colorstr = self.note.tags[0].color
            if color.parse(colorstr):
                cairo_context.rectangle(0, 0, rectangle_size.width,
                                        rectangle_size.height)
                cairo_context.set_source_rgb(color.red, color.green,
                                             color.blue)
                cairo_context.fill()

    def _on_draw_multicolor_ribbon(self, widget, cairo_context):
        rectangle_size = widget.get_allocated_size()[0]
        if self.note.tags:
            colors = [Gdk.RGBA() for i in range(len(self.note.tags))]
            colorsstr = [tag.color for tag in self.note.tags]
            dy = rectangle_size.height / len(self.note.tags)
            radius = rectangle_size.width / 2
            for i, color in enumerate(colors):
                if color.parse(colorsstr[i]):
                    # cairo_context.save()
                    # Circles
                    # dy = (rectangle_size.height) / (len(self.note.tags) + 1)
                    # cairo_context.arc(radius, (i + 1) * dy, radius,
                                      # 0, 2 * 3.14)
                    # Rectangles
                    cairo_context.rectangle(0, i * dy,
                                            rectangle_size.width, dy)
                    cairo_context.set_source_rgb(color.red, color.green,
                                                 color.blue)
                    cairo_context.fill()
                    # cairo_context.restore()

    def _generate_fancy_date(self, date):
        today = datetime.date.today()
        if date == today:
            return 'today'
        elif (date - today).days == -1:
            return 'yesterday'
        elif date.isocalendar()[1] == today.isocalendar()[1]:
            # days[date.weekday()]
            return 'this week'
        elif date.isocalendar()[1] == today.isocalendar()[1] - 1:
            return 'last week'
        else:
            return date.isoformat()

    def on_edit(self):
        """
        When edit button is toggled, switch
        to markdown editor or html viewer
        """
        # Switch to edit mode
        self.editor.show_in_stack()
        self.editor.load_note(self.note, self.tagslist)

    def on_view(self):
        # Switch to viewing mode
        self.htmlview.show_in_stack()
        self.htmlview.update(self.note)

    def on_save(self):
        self.editor.write_note(self.note)
        self.update()

    def on_select(self):
        """
        When line is selected, show the html view
        """
        self.htmlview.show_in_stack()
        self.htmlview.update(self.note)

    def on_delete(self):
        listbox = self.get_parent()
        listbox.remove(self)
        self.note.status = 'deleted-' + self.note.status
        self.destroy()
        listbox.queue_draw()

    def on_starred(self):
        self.note.status = 'starred'
        self.changed()

    def on_normal(self):
        self.note.status = 'normal'
        self.changed()

    def generate_tags_string(self, note):
        string = ''
        for tag in note.tags:
            rematch = COLOR_PARSER.fullmatch(tag.color)
            if rematch is not None:
                rgb = [int(item) for item in rematch.groups()]
                color = "#{0:02x}{1:02x}{2:02x}".format(*rgb)
            string += '<span color="' + color + '">' + '#' + tag.name + '</span>  '
        return string

    def update(self):
        self.title_label.set_text(self.note.title)
        self.summary_label.set_text(self.note.summary)
        date_str = self._generate_fancy_date(self.note.last_modified.date())
        self.date_label.set_text(date_str)
        # self.tags_label.set_markup(self.generate_tags_string(self.note))
        self.queue_draw()
