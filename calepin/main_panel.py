# Standard library modules
import datetime
import os
# External modules
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GObject, GtkSource, Gdk, Gio, GLib
from gi.repository import WebKit2
# Calepin modules
from .viewers import Editor
from .view_panel import ViewerPanel
from .edit_panel import EditPanel
from .tagseditor import TagsEditor


class MainPanel():
    __name__ = '__mainpanel__'

    def __init__(self, parent, ui_builder, app_prefs):
        # Instances of the 3 major UI components:
        # Markdown editor, HTML viewer and list of notes (left side)
        self.editor_panel = EditPanel(parent, ui_builder, app_prefs)
        self.viewer_panel = ViewerPanel(
            parent, ui_builder, self.editor_panel.editor, app_prefs)
        self.parent = parent
        self.app_prefs = app_prefs
        self.ui_builder = ui_builder
        # UI elements
        self.note_popmenu = ui_builder.get_object('NoteMenuDisplayPopover')
        self.popmenu = ui_builder.get_object('PopoverMenu')
        self.starring_button = ui_builder.get_object('StarringButton')
        self.header_bar = ui_builder.get_object('HeaderBar')
        # Restore previous appareance state
        paned_view = ui_builder.get_object('paned_view')
        position = self.app_prefs.get_panedview_width()
        paned_view.set_position(position)
        # In-app notifications
        self.notification_revealer = ui_builder.get_object(
            'NotificationRevealer')
        undo_button = ui_builder.get_object('UndoDelete')
        undo_button.connect('clicked', self.on_undo_delete)
        close_button = ui_builder.get_object('NotificationCloseButton')
        close_button.connect('clicked', self.on_close_notification)
        #
        delsel_button = ui_builder.get_object('delete_notes_selection')
        delsel_button.connect('clicked', self.on_delete_selection)
        #
        self.viewer_panel.listnotes.connect(
            'notify::has-unsaved-changes', self.on_note_changed)
        #
        self.connect_actions(parent)
        # Force update of zoom level button display
        self.on_zoom_level_changed()

    def display(self):
        self._assemble_header_bar()
        main_stack = self.ui_builder.get_object('main_stack')
        main_stack.set_visible_child_name('view_browse_page')

    def _assemble_header_bar(self):
        item = self.ui_builder.get_object('add_button')
        item.show()
        item = self.ui_builder.get_object('StarringButton')
        item.show()
        item = self.ui_builder.get_object('edit_button')
        item.show()
        item = self.ui_builder.get_object('NoteMenuButton')
        item.show()
        item = self.ui_builder.get_object('new_file_button')
        item.hide()

    def load_file(self, filename):
        self.viewer_panel.load_file(filename)

    def enable_main_actions(self):
        actions_group_names = ['file', 'notes', 'view']
        for action_group_name in actions_group_names:
            action_group = self.parent.get_action_group(action_group_name)
            if action_group is not None:
                for action_name in action_group.list_actions():
                    action_group.lookup_action(action_name).set_enabled(True)

    def save_settings(self):
        # save uid of selected note
        self.app_prefs.set_last_note_uid(
            self.viewer_panel.listnotes.get_selected())
        # save name of currently opened file
        self.app_prefs.set_current_file(
            str(self.viewer_panel.listnotes.notes.current_target))
        # save zoom level
        self.app_prefs.webview_zoom_level = self.viewer_panel.viewer.htmlviewer.webview.get_zoom_level()
        # save width of left panel in paned view
        paned_view = self.ui_builder.get_object('paned_view')
        position = paned_view.get_position()
        self.app_prefs.set_panedview_width(position)

    def select_note(self, uid):
        self.viewer_panel.listnotes.select(uid)

    def connect_actions(self, parent):
        add_action = parent.get_action_group('notes').lookup_action('add')
        add_action.connect("change-state", self.on_add_note)
        edit_action = parent.get_action_group('notes').lookup_action('edit')
        edit_action.connect("change-state", self.on_edit_note)
        del_action = parent.get_action_group('notes').lookup_action('delete')
        del_action.connect("activate", self.on_delete_note)
        star_action = parent.get_action_group('notes').lookup_action('starred')
        star_action.connect("change-state", self.on_star_note)
        tags_action = parent.get_action_group(
            'notes').lookup_action('tagsedit')
        tags_action.connect("activate", self.on_edit_tags)

        zoom_in_action = parent.get_action_group(
            'view').lookup_action('zoomin')
        zoom_in_action.connect(
            'activate', self.viewer_panel.viewer.htmlviewer.zoom_in)
        zoom_in_action.connect(
            'activate', self.editor_panel.editor.preview.zoom_in)
        zoom_in_action.connect('activate', self.on_zoom_level_changed)

        zoom_d_action = parent.get_action_group(
            'view').lookup_action('zoom-default')
        zoom_d_action.connect(
            'activate', self.viewer_panel.viewer.htmlviewer.zoom_default)
        zoom_d_action.connect(
            'activate', self.editor_panel.editor.preview.zoom_default)
        zoom_d_action.connect('activate', self.on_zoom_level_changed)

        zoom_out_action = parent.get_action_group(
            'view').lookup_action('zoomout')
        zoom_out_action.connect(
            'activate', self.viewer_panel.viewer.htmlviewer.zoom_out)
        zoom_out_action.connect(
            'activate', self.editor_panel.editor.preview.zoom_out)
        zoom_out_action.connect('activate', self.on_zoom_level_changed)

        save_action = parent.get_action_group('file').lookup_action('save')
        save_action.connect('activate', self.on_save_notes)

    def on_add_note(self, action, param):
        newitem = self.viewer_panel.listnotes.add()
        self.viewer_panel.listnotes.select(newitem.note.uid)
        edit_action = self.parent.get_action_group(
            'notes').lookup_action('edit')
        edit_action.change_state(GLib.Variant.new_boolean(True))

    def on_edit_note(self, action, param):
        selection = self.viewer_panel.notes_list.get_selected_rows()
        if len(selection) > 0:
            current_note = selection[0]
            # If toggling OUT from edit mode
            if action.get_state().equal(GLib.Variant.new_boolean(True)):
                action.set_state(GLib.Variant.new_boolean(False))
                add_action = self.parent.get_action_group(
                    'notes').lookup_action('add')
                add_action.set_enabled(True)
                # Update listnotes
                current_note.update()
                # Display note
                current_note.on_view()
            # If toggling INTO edit mode
            else:
                action.set_state(GLib.Variant.new_boolean(True))
                add_action = self.parent.get_action_group(
                    'notes').lookup_action('add')
                add_action.set_enabled(False)
                current_note.on_edit()
                # Disable editing action so that user can't toggle back in
                # uncontrolled manner
                action.set_enabled(False)

    def on_delete_note(self, *args):
        """
        Delete the note that is currently selected
        """
        # Get the note
        current_note = self.viewer_panel.listnotes.listbox.get_selected_rows()[
            0]
        edit_action = self.parent.get_action_group(
            'notes').lookup_action('edit')
        # If currently in edition mode, switch back to view mode
        if edit_action.get_state().equal(GLib.Variant.new_boolean(True)):
            edit_action.change_state(GLib.Variant.new_boolean(False))
        # If note popmenu is shown, hide it
        if self.note_popmenu.is_visible():
            self.note_popmenu.hide()
        # Delete the note
        self.viewer_panel.listnotes.delete([current_note])
        # Show notification
        self.notification_revealer.set_reveal_child('True')
        # Timeout to hide notification after some time
        GLib.timeout_add(5000, self.on_notification_timeout, None)
        # Clear the note view
        self.viewer_panel.viewer.clear()

    def on_star_note(self, action, value):
        if value:
            self.viewer_panel.listnotes.star()
            self.starring_button.set_property('icon',
                                              Gio.ThemedIcon.new('starred-symbolic'))
        else:
            self.viewer_panel.listnotes.unstar()
            self.starring_button.set_property('icon',
                                              Gio.ThemedIcon.new('non-starred-symbolic'))
        action.set_state(value)

    def on_save_notes(self, action, param):
        if self.popmenu.is_visible():
            self.popmenu.hide()
        # Save all notes
        self.viewer_panel.listnotes.save()

    def on_note_changed(self, object, gparamstr):
        title = self.header_bar.get_title()
        if title[0] != '*' and self.viewer_panel.listnotes.has_unsaved_changes:
            self.header_bar.set_title('*' + title)
        elif title[0] == '*' and not self.viewer_panel.listnotes.has_unsaved_changes:
            self.header_bar.set_title(title[1:])

    def on_zoom_level_changed(self, *args):
        current_zoom = self.viewer_panel.viewer.webview.get_zoom_level()
        zoom_str = '{:3.0f}%'.format(current_zoom * 100)
        button = self.ui_builder.get_object('zoom_deflt_button')
        button.set_label(zoom_str)

    def on_notification_timeout(self, *args):
        if self.notification_revealer.get_child_revealed():
            self.notification_revealer.set_reveal_child(False)
        return False

    def on_close_notification(self, *args):
        self.notification_revealer.set_reveal_child(False)

    def on_undo_delete(self, *args):
        self.viewer_panel.listnotes.undo_last_delete()
        self.notification_revealer.set_reveal_child(False)

    def on_delete_selection(self, *args):
        notes = self.viewer_panel.listnotes.listbox.get_selected_rows()
        self.viewer_panel.listnotes.delete(notes)
        # Show notification
        self.notification_revealer.set_reveal_child('True')
        # Clear the note view
        self.viewer_panel.viewer.clear()

    def on_edit_tags(self, *args):
        dialog = TagsEditor(
            self.parent, self.viewer_panel.listnotes.notes.tagslist)
        dialog.connect('notify::tags-modified',
                       self.viewer_panel.listnotes._update_all_tags)
        dialog.connect('notify::tags-modified',
                       self.editor_panel.editor.tags_picker.on_tagslist_update)
        dialog.connect('notify::tags-modified',
                       self.viewer_panel.viewer.on_tags_updated)
        dialog.show()

    def on_close(self):
        self.viewer_panel.listnotes.clear()
